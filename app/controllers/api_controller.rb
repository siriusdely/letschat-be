class ApiController < ActionController::API
  before_action :authenticate

  private

  def authenticate
    user = User.find(decoded_token[:id])
  rescue
    render json: { error: 'Invalid token' }, status: :unauthorized
  end

  def decoded_token
    return @decoded_token if @decoded_token
    headers = request.headers['Authorization']
    authorization = headers.split(' ').last if headers.present?
    @decoded_token ||= JsonWebToken.decode(authorization) if authorization
  rescue
    @decoded_token = nil
  end
end
