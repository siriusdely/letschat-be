class Api::V1::AuthenticationController < ApiController
  skip_before_action :authenticate, only: :login

  def login
    if user
      token = JsonWebToken.encode id: user.id.to_str
      render json: { token: token, user: user }
    else
      render json: { error: 'Invalid credentials' }, status: :unauthorized
    end
  end

  private

  def authentication_params
    params.permit(:email, :password)
  end

  def user
    user = User.find_by(email: params[:email])
    return user if user and user.valid_password?(params[:password])
  rescue
    return nil
  end
end
