class Api::V1::MessagesController < ApiController
  def index
    messages = Message.all.entries
    render json: { messages: messages }
  end
end
