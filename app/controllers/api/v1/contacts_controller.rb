class Api::V1::ContactsController < ApiController
  def index
    contacts = User.all.entries
    render json: { contacts: contacts }
  end
end
