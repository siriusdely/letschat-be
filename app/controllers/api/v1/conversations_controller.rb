class Api::V1::ConversationsController < ApiController
  def index
    conversations = Conversation.all.entries
    render json: { conversations: conversations }
  end
end
