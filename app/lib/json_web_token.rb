class JsonWebToken
  class << self
    SECRET = Rails.application.secrets.secret_key_base

    def encode(payload, expires = 24.hours.from_now)
      payload[:expires] = expires.to_i
      JWT.encode(payload, SECRET)
    end

    def decode(token)
      body = JWT.decode(token, SECRET)[0]
      HashWithIndifferentAccess.new body
    end
  end
end
