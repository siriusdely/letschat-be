class Message
  include Mongoid::Document

  field :content, type: String
  belongs_to :conversation

  validates_presence_of :content
end
