Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # devise_for :users

  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      post 'users/register', to: 'users#create'
      post 'auth/login', to: 'authentication#login'

      resources :contacts, only: [:index, :create]

      resources :conversations, only: [:index, :create] do
        resources :messages, only: [:index, :create]
      end
    end
  end
end
